import express from 'express';


import AdminController from '../controllers/AdminController';
// import CategoriesController from '../controllers/CategoryController';
// import CustomerController from '../controllers/CustomerController';
// import UserController from '../controllers/UserController';
// import VendorController from '../controllers/VendorController';
// import ItemController from '../controllers/ItemController';
// import ReviewController from '../controllers/ReviewController';
// import QuestionController from '../controllers/QuestionController';
// import CarItemController from '../controllers/CartItemController';
// import ItemModelController from '../controllers/ItemModelController';
// import TransactionController from '../controllers/TransactionController';
// import UploadController from '../controllers/UploadController';
// import VendorMailController from '../controllers/VendorMailController';
// import ReportController from '../controllers/ReportController';
// import TestDataGenerator from '../controllers/TestDataGenerator';


let router = express.Router();

router.use('/admins', AdminController);
// router.use('/categories', CategoriesController);
// router.use('/customers', CustomerController);
// router.use('/users', UserController);
// router.use('/vendors', VendorController);

// router.use('/items', ItemController);
// router.use('/reviews',ReviewController);
// router.use('/questions',QuestionController);

// router.use('/cartitems', CarItemController);
// router.use('/itemmodels', ItemModelController);
// router.use('/transactions', TransactionController);
// router.use('/vendormails', VendorMailController);
// router.use('/reports', ReportController);

// router.use('/upload',UploadController);

// router.use('/test', TestDataGenerator);

export default router;