import {model, Schema} from 'mongoose';

let SubCategoryAdminSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        lowercase: true,
        required: true,
        enum: [
            'male',
            'female'
        ]
    },
    subcategory_id: {
        type: Schema.Types.ObjectId,
        ref: 'SubCategory',
        required: true
    }
}, {collection: 'subcategory_admins'});

export default model('SubCategoryAdmin', SubCategoryAdminSchema);