import async from 'async';
import express from 'express';
import passport from 'passport';

import User from '../models/User';
import SubCategory from '../models/SubCategory';
import SubCategoryAdmin from '../models/SubCategoryAdmin';

import logger from '../configs/logger';
import config from '../configs/config';
import authMiddleware from '../middlewares/AuthMiddleware';
import { kinds, createError, logError, errorMessages } from '../configs/errors';

let router = express.Router();

/**
 * Retrieve All SubCategoriesAdmins
 */
router.get('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    SubCategoryAdmin.find({}, (error, sub_category_admins) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "SubCategory Admin",
                errorMessages.SUBCATEGORYADMIN_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(sub_category_admins);
        }
    });
});

/**
 * Retrieve SubCategoryAdmins by ID
 */
router.get('/:id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    SubCategoryAdmin.findById(request.params.id, (error, sub_category_admin) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "SubCategory Admin",
                errorMessages.SUBCATEGORYADMIN_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else if (!sub_category_admin) {
            let errorMessage = createError(
                errorMessages.SUBCATEGORYADMIN_RETRIEVAL_FAILED,
                "GET",
                errorMessages.SUBCATEGORYADMIN_NOT_FOUND
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(sub_category_admin);
        }
    });
});

/**
 * Create SubCategoryAdmin
 * @param  {email, password, first_name, last_name, gender, sub_name, sub_description, category_id}
 */
router.post('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.sub_admin), 
    (request, response) => {

        async.waterfall([
            (done) => {
                let newSubCategory = new SubCategory();
                newSubCategory.name = request.body.sub_name;
                newSubCategory.description = request.body.sub_description;
                newSubCategory.category_id = request.body.category_id
                newSubCategory.save((error, sub_category) => {
                    if (error) {
                        let errorMessage = processMongooseErrors(
                            "SubCategory",
                            errorMessages.SUBCATEGORY_REGISTRATION_FAILED,
                            "POST",
                            error
                        );

                        done(errorMessage);
                    }
                    else {
                        logger.info("New Sub-Category Registered");
                        done(null, sub_category);
                    }
                });
            },

            (sub_category, done) => {
                let newUser = new User();
                newUser.email = request.body.email;
                newUser.user_type = config.roles.sub_admin;
                newUser.hash(request.body.password, config.security.saltRound, (error, hash) => {
                    if (error) {
                        let errorMessage = createError(
                            errorMessages.SUBCATEGORYADMIN_REGISTRATION_FAILED,
                            'POST',
                            errorMessages.PASSWORD_IS_REQUIRED
                        );
    
                        done(errorMessage);
                    }   
                    else {
                        newUser.password = hash;
                        newUser.save((error, user) => {
                            if (error) {
                                let errorMessage = processMongooseErrors(
                                    "User",
                                    errorMessages.SUBCATEGORYADMIN_REGISTRATION_FAILED,
                                    "POST",
                                    error
                                );
                                done(errorMessage);
                            }
                            else {
                                done(null, user, sub_category);
                            }
                        });
                    }
                });
            },
            (user, sub_category, done) => {
                let newSubCategoryAdmin = new SubCategoryAdmin();
                newSubCategoryAdmin.user_id = user;
                newSubCategoryAdmin.subcategory_id = sub_category;
                newSubCategoryAdmin.first_name = request.body.first_name;
                newSubCategoryAdmin.last_name = request.body.last_name;
                newSubCategoryAdmin.gender = request.body.gender;
            
                newSubCategoryAdmin.save((error, subCategoryAdmin) => {
                    if (error) {
                        rollback(user);
    
                        let errorMessage = processMongooseErrors(
                            "SubCategory Admin",
                            errorMessages.SUBCATEGORYADMIN_REGISTRATION_FAILED,
                            "POST",
                            error
                        );
                        done(errorMessage);
                    }
                    else {
                        logger.info("New Sub-Category Admin Registered");
                        return response.status(201).json(subCategoryAdmin);
                    }
                });
            }
        ], (error) => {
            logError(error);
            return response.status(400).send(error);
        })
});

/**
 * Change Password
 * @param  {currentPassword, password} 
 */
router.post('/changePassword', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.sub_admin), 
    (request, response) => {
    
    let user = request.user;

    user.comparePassword(request.body.currentPassword, user.password, (error, status) => {
        if (status) {
            user.hash(request.body.password, config.security.saltRound, (error, hash) => {
                if (error) {
                    let errorMessage = createError(
                        errorMessages.SUBCATEGORYADMIN_CHANGE_PASSWORD_FAILED,
                        'POST',
                        errorMessages.PASSWORD_IS_REQUIRED
                    );

                    response.status(400).json(errorMessage);
                }   
                else {
                    user.password = hash;
                    user.save((error, updatedUser) => {
                        if (error) {
                            let errorMessage = processMongooseErrors(
                                "User",
                                errorMessages.SUBCATEGORYADMIN_CHANGE_PASSWORD_FAILED,
                                "POST",
                                error
                            );
                            response.status(400).json(errorMessage);
                        }
                        else {
                            response.status(200).json(updatedUser);
                        }
                    });
                }
            });
        }
        else {
            let errorMessage = createError(
                errorMessages.SUBCATEGORYADMIN_CHANGE_PASSWORD_FAILED,
                'POST',
                errorMessages.INVALID_CURRENT_PASSWORD
            );
 
            response.status(400).json(errorMessage);
        }
    });
});

let processMongooseErrors = (resource, message, method, error) => {
    let errorList = [];

    if (error.name === "ValidationError") {
        errorList = processValidationErrors(error);
    }
    else if (error.name === "CastError") {
        errorList.push(errorMessages.SUBCATEGORYADMIN_NOT_FOUND);
    }
    else {
        errorList.push(errorMessages.UNKNOWN_ERROR);
    }
    return createError(message, method, errorList);
}

let processValidationErrors = (error) => {
    let errorList = [];

    if (error.errors.name) {
        if (error.errors.name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.SUBCATEGORY_NAME_IS_REQUIRED);
        }
    }

    if (error.errors.description) {
        if (error.errors.description.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.SUBCATEGORY_DESCRIPTION_IS_REQUIRED);
        }
    }

    return errorList;
}

export default router;