import async from 'async';
import express from 'express';
import passport from 'passport';

import Category from '../models/Category';
import SubCategory from '../models/SubCategory';

import logger from '../configs/logger';
import config from '../configs/config';
import authMiddleware from '../middlewares/AuthMiddleware';
import { kinds, createError, logError, errorMessages } from '../configs/errors';

let router = express.Router();

/**
 * Retrieve All SubCategories
 */
router.get('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    SubCategory.find({}, (error, sub_categories) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Category",
                errorMessages.SUBCATEGORY_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(sub_categories);
        }
    });
});

/**
 * Retrieve SubCategory by ID
 */
router.get('/:id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin, config.roles.sub_admin), 
    (request, response) => {

    SubCategory.findById(request.params.id, (error, sub_category) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Category",
                errorMessages.SUBCATEGORY_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else if (!sub_category) {
            let errorMessage = createError(
                errorMessages.SUBCATEGORY_RETRIEVAL_FAILED,
                "GET",
                errorMessages.SUBCATEGORY_NOT_FOUND
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(sub_category);
        }
    });
});

/**
 * Create SubCategory
 * @param  {name, description, category_id}
 */
router.post('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.sub_admin), 
    (request, response) => {

    let newSubCategory = new SubCategory();
    newSubCategory.name = request.body.name;
    newSubCategory.description = request.body.description;
    newSubCategory.category_id = request.body.category_id
    newSubCategory.save((error, sub_category) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "SubCategory",
                errorMessages.SUBCATEGORY_REGISTRATION_FAILED,
                "POST",
                error
            );
            response.status(400).send(errorMessage);
        }
        else {
            logger.info("New Sub-Category Registered");
            response.status(200).json(sub_category);
        }
    });
});

/**
 * Update Category
 * @param  {name, description}
 */
router.put('/:id', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    async.waterfall([
        (done) => {
            SubCategory.findById(request.params.id, (error, sub_category) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "SubCategory",
                        errorMessages.SUBCATEGORY_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else if (!sub_category) {
                    let errorMessage = createError(
                        errorMessages.SUBCATEGORY_UPDATE_FAILED,
                        "PUT",
                        errorMessages.SUBCATEGORY_NOT_FOUND
                    );
                    done(errorMessage);
                }
                else {
                    done(null, sub_category);
                }
            });
        },
        (sub_category, done) => {
            sub_category.name = request.body.name;
            sub_category.description = request.body.description;
            sub_category.save((error, updated_sub_Category) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Category",
                        errorMessages.SUBCATEGORY_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    response.status(200).json(updated_sub_Category);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error); 
    });
});


let processMongooseErrors = (resource, message, method, error) => {
    let errorList = [];

    if (error.name === "ValidationError") {
        errorList = processValidationErrors(error);
    }
    else if (error.name === "CastError") {
        errorList.push(errorMessages.CATEGORY_NOT_FOUND);
    }
    else {
        errorList.push(errorMessages.UNKNOWN_ERROR);
    }
    return createError(message, method, errorList);
}

let processValidationErrors = (error) => {
    let errorList = [];

    if (error.errors.name) {
        if (error.errors.name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.CATEGORY_NAME_IS_REQUIRED);
        }
    }

    if (error.errors.description) {
        if (error.errors.description.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.CATEGORY_DESCRIPTION_IS_REQUIRED);
        }
    }

    return errorList;
}

export default router;