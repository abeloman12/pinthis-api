import {model, Schema} from 'mongoose';

let PostSchema = new Schema({
    subcategory_id: {
        type: Schema.Types.ObjectId,
        ref: 'SubCategory',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    image_paths:[{
        type: String
    }],
    created_at:{
        type:Date,
        required: true,
        default:Date.now
    },
    updated_at:{
        type:Date,
        required: true,
        default:Date.now
    }


}, {collection: 'posts'});

export default model('Post', PostSchema);