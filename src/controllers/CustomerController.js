import async from 'async';
import express from 'express';
import passport from 'passport';

import User from '../models/User';
import Customer from '../models/Customer';

import logger from '../configs/logger';
import config from '../configs/config';
import authMiddleware from '../middlewares/AuthMiddleware';
import { kinds, createError, logError, errorMessages } from '../configs/errors';

let router = express.Router();

/**
 * Retrieve All Customers
 */
router.get('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Customer.find({}).populate('user_id').exec((error, customers) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Customer",
                errorMessages.CUSTOMER_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(customers);
        }
    });
});

/**
 * Retrieve Number of Customers
 */
router.get('/count', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Customer.countDocuments({}, (error, vendorsCount) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Customer",
                errorMessages.CUSTOMER_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(vendorsCount);
        }
    });
});

/**
 * Retrieve a Customer
 */
router.get('/:id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Customer.findById(request.params.id).populate('user_id').exec((error, customer) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Customer",
                errorMessages.CUSTOMER_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else if (!customer) {
            let errorMessage = createError(
                errorMessages.CUSTOMER_RETRIEVAL_FAILED,
                "GET",
                errorMessages.CUSTOMER_NOT_FOUND
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.json(customer);
        }
    });
});

/**
 * Retrieve a Customer from user_id
 */
router.get('/user/:user_id',
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.customer), 
    (request,response) => {
        Customer.findOne({user_id: request.params.user_id}).populate('user_id').exec((error, customer) => {
            if (error) {
                let errorMessage = processMongooseErrors(
                    "Customer",
                    errorMessages.CUSTOMER_RETRIEVAL_FAILED,
                    "GET",
                    error
                );
                response.status(404).json(errorMessage);
            }
            else if (!customer) {
                let errorMessage = createError(
                    errorMessages.CUSTOMER_RETRIEVAL_FAILED,
                    "GET",
                    errorMessages.CUSTOMER_NOT_FOUND
                );
                response.status(404).json(errorMessage);
            }
            else {
                response.json(customer);
            }
        });
});

/**
 * Create Customer
 * @param  {email, password, username, first_name, last_name, gender}
 */
router.post('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {
    
    async.waterfall([
        (done) => {
            let newUser = new User();
            newUser.email = request.body.email;
            newUser.user_type = config.roles.customer;
            newUser.hash(request.body.password, config.security.saltRound, (error, hash) => {
                if (error) {
                    let errorMessage = createError(
                        errorMessages.CUSTOMER_REGISTRATION_FAILED,
                        'POST',
                        errorMessages.PASSWORD_IS_REQUIRED
                    );

                    done(errorMessage);
                }   
                else {
                    newUser.password = hash;
                    newUser.save((error, user) => {
                        if (error) {
                            let errorMessage = processMongooseErrors(
                                "User",
                                errorMessages.CUSTOMER_REGISTRATION_FAILED,
                                "POST",
                                error
                            );
                            done(errorMessage);
                        }
                        else {
                            done(null, user);
                        }
                    });
                }
            });
        },
        (user, done) => {
            let newCustomer = new Customer();
            newCustomer.user_id = user;
            newCustomer.username = request.body.username;
            newCustomer.first_name = request.body.first_name;
            newCustomer.last_name = request.body.last_name;
            newCustomer.gender = request.body.gender;
        
            newCustomer.save((error, customer) => {
                if (error) {
                    rollback(user);

                    let errorMessage = processMongooseErrors(
                        "Customer",
                        errorMessages.CUSTOMER_REGISTRATION_FAILED,
                        "POST",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    logger.info("New Customer Registered");
                    return response.status(201).json(customer);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error);
    })
    
});


/**
 * Update Customer
 * @param  {email, username, first_name, last_name, gender}
 */
router.put('/', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.customer), 
    (request, response) => {

    async.waterfall([
        (done) => {
            let user = request.user;

            user.email = request.body.email;
            user.save((error, updatedUser) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "User",
                        errorMessages.CUSTOMER_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    done(null, updatedUser);
                }
            });
        },
        (user, done) => {
            Customer.findOne({user_id: user}).populate('user_id').exec((error, customer) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Customer",
                        errorMessages.CUSTOMER_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else if (!customer) {
                    let errorMessage = createError(
                        errorMessages.CUSTOMER_UPDATE_FAILED,
                        "PUT",
                        errorMessages.CUSTOMER_NOT_FOUND
                    );
                    done(errorMessage);
                }
                else {
                    done(null, user, customer);
                }
            });   
        },
        (user, customer, done) => {
            customer.username = request.body.username;
            customer.first_name = request.body.first_name;
            customer.last_name = request.body.last_name;
            customer.gender = request.body.gender;
            
            customer.save((error, updatedCustomer) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Customer",
                        errorMessages.CUSTOMER_UPDATE_FAILED,
                        "POST",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    response.status(200).json(updatedCustomer);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error);
    });
});

/**
 * Change Password
 * @param  {currentPassword, password} 
 */
router.post('/changePassword', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.customer), 
    (request, response) => {
    
    let user = request.user;

    user.comparePassword(request.body.currentPassword, user.password, (error, status) => {
        if (status) {
            user.hash(request.body.password, config.security.saltRound, (error, hash) => {
                if (error) {
                    let errorMessage = createError(
                        errorMessages.CUSTOMER_CHANGE_PASSWORD_FAILED,
                        'POST',
                        errorMessages.PASSWORD_IS_REQUIRED
                    );

                    response.status(400).json(errorMessage);
                }   
                else {
                    user.password = hash;
                    user.save((error, updatedUser) => {
                        if (error) {
                            let errorMessage = processMongooseErrors(
                                "User",
                                errorMessages.CUSTOMER_CHANGE_PASSWORD_FAILED,
                                "POST",
                                error
                            );
                            response.status(400).json(errorMessage);
                        }
                        else {
                            response.status(200).json(updatedUser);
                        }
                    });
                }
            });
        }
        else {
            let errorMessage = createError(
                errorMessages.CUSTOMER_CHANGE_PASSWORD_FAILED,
                'POST',
                errorMessages.INVALID_CURRENT_PASSWORD
            );
 
            response.status(400).json(errorMessage);
        }
    });
});


let rollback = (...items) => {
    items.forEach(item => {
        item.remove();
    });
}

let processMongooseErrors = (resource, message, method, error) => {
    let errorList = [];

    if (error.name === "ValidationError") {
        errorList = processValidationErrors(error);
    }
    else if (error.code === 11000) {
        errorList.push(errorMessages.CUSTOMER_WITH_EMAIL_ALREADY_EXISTS);
    }
    else if (error.name === "CastError") {
        errorList.push(errorMessages.CUSTOMER_NOT_FOUND);
    }
    else {
        errorList.push(errorMessages.UNKNOWN_ERROR);
    }
    return createError(message, method, errorList);
}

let processValidationErrors = (error) => {
    let errorList = [];

    if (error.errors.email) {
        if (error.errors.email.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.EMAIL_IS_REQUIRED);
        }
    }

    if (error.errors.password) {
        if (error.errors.password.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.PASSWORD_IS_REQUIRED);
        }
    }

    if (error.errors.user_type) {
        if (error.errors.user_type.kind === kinds.ENUM) {
            errorList.push(errorMessages.INCORRECT_USER_TYPE);
        }
    }

    if (error.errors.user_id) {
        if (error.errors.user_id.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.USER_ID_REQUIRED);
        }
    }

    if (error.errors.username) {
        if (error.errors.username.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.CUSTOMER_USERNAME_REQUIRED);
        }
    }

    if (error.errors.first_name) {
        if (error.errors.first_name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.FIRST_NAME_REQUIRED);
        }
    }

    if (error.errors.last_name) {
        if (error.errors.last_name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.LAST_NAME_REQUIRED);
        }
    }

    if (error.errors.gender) {
        if (error.errors.gender.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.GENDER_REQUIRED);
        }
        if (error.errors.gender.kind === kinds.ENUM) {
            errorList.push(errorMessages.INCORRECT_GENDER);
        }
    }

    return errorList;
}

export default router;