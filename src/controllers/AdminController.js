import async from 'async';
import express from 'express';
import passport from 'passport';

import User from '../models/User';
import Admin from '../models/Admin';

import logger from '../configs/logger';
import config from '../configs/config';
import authMiddleware from '../middlewares/AuthMiddleware';
import { kinds, createError, logError, errorMessages } from '../configs/errors';

let router = express.Router();

/**
 * Retrieve All Admins
 */
router.get('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Admin.find({}).populate('user_id').exec((error, admins) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Admin",
                errorMessages.ADMIN_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(admins);
        }
    });
});


/**
 * Retrieve an Admin
 */
router.get('/:id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Admin.findById(request.params.id).populate('user_id').exec((error, admin) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Admin",
                errorMessages.ADMIN_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else if (!admin) {
            let errorMessage = createError(
                errorMessages.ADMIN_RETRIEVAL_FAILED,
                "GET",
                errorMessages.ADMIN_NOT_FOUND
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.json(admin);
        }
    });
});


/**
 * Retrieve an Admin using User_ID
 */
router.get('/user/:id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Admin.findOne({user_id: request.params.id}).populate('user_id').exec((error, admin) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Admin",
                errorMessages.ADMIN_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else if (!admin) {
            let errorMessage = createError(
                errorMessages.ADMIN_RETRIEVAL_FAILED,
                "GET",
                errorMessages.ADMIN_NOT_FOUND
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.json(admin);
        }
    });
});

/**
 * Create Admin
 * @param  {email, password, first_name, last_name, genders}
 */
router.post('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {
    
    async.waterfall([
        (done) => {
            let newUser = new User();
            newUser.email = request.body.email;
            newUser.user_type = config.roles.admin;
            newUser.hash(request.body.password, config.security.saltRound, (error, hash) => {
                if (error) {
                    let errorMessage = createError(
                        errorMessages.ADMIN_REGISTRATION_FAILED,
                        'POST',
                        errorMessages.PASSWORD_IS_REQUIRED
                    );

                    done(errorMessage);
                }   
                else {
                    newUser.password = hash;
                    newUser.save((error, user) => {
                        if (error) {
                            let errorMessage = processMongooseErrors(
                                "User",
                                errorMessages.ADMIN_REGISTRATION_FAILED,
                                "POST",
                                error
                            );
                            done(errorMessage);
                        }
                        else {
                            done(null, user);
                        }
                    });
                }
            });
        },
        (user, done) => {
            let newAdmin = new Admin();
            newAdmin.user_id = user;
            newAdmin.first_name = request.body.first_name;
            newAdmin.last_name = request.body.last_name;
            newAdmin.gender = request.body.gender;

            newAdmin.save((error, admin) => {
                if (error) {
                    rollback(user);

                    let errorMessage = processMongooseErrors(
                        "Admin",
                        errorMessages.ADMIN_REGISTRATION_FAILED,
                        "POST",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    logger.info("New Admin Registered");
                    return response.status(201).json(admin);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error);
    })
    
});


/**
 * Update Admin
 * @param  {email, first_name, last_name, gender} 
 */
router.put('/', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    async.waterfall([
        (done) => {
            let user = request.user;

            user.email = request.body.email;
            user.save((error, updatedUser) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "User",
                        errorMessages.ADMIN_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    done(null, updatedUser);
                }
            });
        },
        (user, done) => {
            Admin.findOne({user_id: user}).populate('user_id').exec((error, admin) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Admin",
                        errorMessages.ADMIN_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else if (!admin) {
                    let errorMessage = createError(
                        errorMessages.ADMIN_UPDATE_FAILED,
                        "PUT",
                        errorMessages.ADMIN_NOT_FOUND
                    );
                    done(errorMessage);
                }
                else {
                    done(null, user, admin);
                }
            });   
        },
        (user, admin, done) => {
            admin.first_name = request.body.first_name;
            admin.last_name = request.body.last_name;
            admin.gender = request.body.gender;

            admin.save((error, updatedAdmin) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Admin",
                        errorMessages.ADMIN_UPDATE_FAILED,
                        "POST",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    response.status(200).json(updatedAdmin);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error);
    });
});

/**
 * Change Password
 * @param  {currentPassword, password} 
 */
router.post('/changePassword', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {
    
    let user = request.user;

    user.comparePassword(request.body.currentPassword, user.password, (error, status) => {
        if (status) {
            user.hash(request.body.password, config.security.saltRound, (error, hash) => {
                if (error) {
                    let errorMessage = createError(
                        errorMessages.ADMIN_CHANGE_PASSWORD_FAILED,
                        'POST',
                        errorMessages.PASSWORD_IS_REQUIRED
                    );

                    response.status(400).json(errorMessage);
                }   
                else {
                    user.password = hash;
                    user.save((error, updatedUser) => {
                        if (error) {
                            let errorMessage = processMongooseErrors(
                                "User",
                                errorMessages.ADMIN_CHANGE_PASSWORD_FAILED,
                                "POST",
                                error
                            );
                            response.status(400).json(errorMessage);
                        }
                        else {
                            response.status(200).json(updatedUser);
                        }
                    });
                }
            });
        }
        else {
            let errorMessage = createError(
                errorMessages.ADMIN_CHANGE_PASSWORD_FAILED,
                'POST',
                errorMessages.INVALID_CURRENT_PASSWORD
            );
 
            response.status(400).json(errorMessage);
        }
    });
});


let rollback = (...items) => {
    items.forEach(item => {
        item.remove();
    });
}

let processMongooseErrors = (resource, message, method, error) => {
    let errorList = [];

    if (error.name === "ValidationError") {
        errorList = processValidationErrors(error);
    }
    else if (error.code === 11000) {
        errorList.push(errorMessages.ADMIN_ALREADY_EXISTS);
    }
    else if (error.name === "CastError") {
        errorList.push(errorMessages.ADMIN_NOT_FOUND);
    }
    else {
        errorList.push(errorMessages.UNKNOWN_ERROR);
    }
    return createError(message, method, errorList);
}

let processValidationErrors = (error) => {
    let errorList = [];

    if (error.errors.email) {
        if (error.errors.email.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.EMAIL_IS_REQUIRED);
        }
    }

    if (error.errors.password) {
        if (error.errors.password.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.PASSWORD_IS_REQUIRED);
        }
    }

    if (error.errors.user_type) {
        if (error.errors.user_type.kind === kinds.ENUM) {
            errorList.push(errorMessages.INCORRECT_USER_TYPE);
        }
    }

    if (error.errors.user_id) {
        if (error.errors.user_id.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.USER_ID_REQUIRED);
        }
    }

    if (error.errors.first_name) {
        if (error.errors.first_name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.FIRST_NAME_REQUIRED);
        }
    }

    if (error.errors.last_name) {
        if (error.errors.last_name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.LAST_NAME_REQUIRED);
        }
    }

    if (error.errors.gender) {
        if (error.errors.gender.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.GENDER_REQUIRED);
        }
        if (error.errors.gender.kind === kinds.ENUM) {
            errorList.push(errorMessages.INCORRECT_GENDER);
        }
    }

    return errorList;
}

export default router;