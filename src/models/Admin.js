import {model, Schema} from 'mongoose';

let AdminSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        lowercase: true,
        required: true,
        enum: [
            'male',
            'female'
        ]
    }
}, {collection: 'admins'});

export default model('Admin', AdminSchema);