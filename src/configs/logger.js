import { createLogger, transports, format } from 'winston';

export default createLogger({
    level: 'info',
    format: format.combine(
        format.timestamp(),
        format.json(),
        format.prettyPrint()
    ),
    transports: [
        new transports.Console(),
        new transports.File({filename: './src/logs/error.log', level: 'error'}),
        new transports.File({filename: './src/logs/info.log', level: 'info'})
    ]
});
