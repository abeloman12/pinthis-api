import async from 'async';
import express from 'express';
import passport from 'passport';

import Category from '../models/Category'

import logger from '../configs/logger';
import config from '../configs/config';
import authMiddleware from '../middlewares/AuthMiddleware';
import { kinds, createError, logError, errorMessages } from '../configs/errors';

let router = express.Router();

/**
 * Retrieve All Categories
 */
router.get('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Category.find({}, (error, categories) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Category",
                errorMessages.CATEGORY_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(categories);
        }
    });
});

/**
 * Retrieve Category by ID
 */
router.get('/:id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Category.findById(request.params.id, (error, category) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Category",
                errorMessages.CATEGORY_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else if (!category) {
            let errorMessage = createError(
                errorMessages.CATEGORY_RETRIEVAL_FAILED,
                "GET",
                errorMessages.CATEGORY_NOT_FOUND
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(category);
        }
    });
});

/**
 * Create Category
 * @param  {name, description}
 */
router.post('/', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    let newCategory = new Category();
    newCategory.name = request.body.name;
    newCategory.description = request.body.description;
    newCategory.save((error, category) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Category",
                errorMessages.CATEGORY_REGISTRATION_FAILED,
                "POST",
                error
            );
            response.status(400).send(errorMessage);
        }
        else {
            logger.info("New Category Registered");
            response.status(200).json(category);
        }
    });
});

/**
 * Update Category
 * @param  {name, description}
 */
router.put('/:id', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    async.waterfall([
        (done) => {
            Category.findById(request.params.id, (error, category) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Category",
                        errorMessages.CATEGORY_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else if (!category) {
                    let errorMessage = createError(
                        errorMessages.CATEGORY_UPDATE_FAILED,
                        "PUT",
                        errorMessages.CATEGORY_NOT_FOUND
                    );
                    done(errorMessage);
                }
                else {
                    done(null, category);
                }
            });
        },
        (category, done) => {
            category.name = request.body.name;
            category.description = request.body.description;
            category.save((error, updatedCategory) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Category",
                        errorMessages.CATEGORY_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    response.status(200).json(updatedCategory);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error); 
    });
});


let processMongooseErrors = (resource, message, method, error) => {
    let errorList = [];

    if (error.name === "ValidationError") {
        errorList = processValidationErrors(error);
    }
    else if (error.code === 11000) {
        if (resource === 'Category') {
            errorList.push(errorMessages.CATEGORY_WITH_PROVIDED_NAME_EXISTS);
        }
    }
    else if (error.name === "CastError") {
        errorList.push(errorMessages.CATEGORY_NOT_FOUND);
    }
    else {
        errorList.push(errorMessages.UNKNOWN_ERROR);
    }
    return createError(message, method, errorList);
}

let processValidationErrors = (error) => {
    let errorList = [];

    if (error.errors.name) {
        if (error.errors.name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.CATEGORY_NAME_IS_REQUIRED);
        }
    }

    if (error.errors.description) {
        if (error.errors.description.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.CATEGORY_DESCRIPTION_IS_REQUIRED);
        }
    }

    return errorList;
}

export default router;