import {model, Schema} from 'mongoose';

let CategorySchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
}, {collection: 'categories'});

export default model('Category', CategorySchema);