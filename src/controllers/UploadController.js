import multer from 'multer';
import express from 'express';
import { createError, logError, errorMessages } from '../configs/errors';

let router = express.Router();

let storage = multer.diskStorage({
    destination: (request, file, cb) => {
        cb(null, 'uploads/images/');
    },
    filename: (request, file, cb) => {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

let upload = multer({storage: storage}).single('photo');

/**
 * Upload Item Images
 */
router.post('/', (request, response) => {
    upload(request, response, (error) => {
        if (error) {
            let errorMessage = createError(
                errorMessages.UPLOAD_FAILURE,
                "POST",
                error
            );
            logError(error);
            return  response.status(400).json(errorMessage);
        }
        else {
            return response.send(request.file.path);
        }
    });
});

export default router;