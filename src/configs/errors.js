import logger from './logger';

export const kinds = {
    REQUIRED: "required",
    NOT_VALID:"notvalid",
    NUMBER_ERROR:"Number",
    MIN_ERROR:"min",
    MAX_ERROR:"max",
    ENUM:"enum",
    DATE:"Date",
    BOOLEAN:"Boolean",
    USER_DEFINED: "user defined"
};

export const createError = function(message, httpMethod, errors) {
    return {
        text: message,
        timestamp: new Date(),
        method: httpMethod,
        errors: errors
    };
};

export const logError = function(errorMessage) {
    logger.error(errorMessage);
};

export const errorMessages = {
    // General Errors
    UNKNOWN_ERROR: "Unknown Error",

    // User Errors
    EMAIL_IS_REQUIRED: "Email is required",
    PASSWORD_IS_REQUIRED: "Password is required",
    INCORRECT_USER_TYPE: "Invalid user type provided",
    USER_ID_REQUIRED: "User Id is required",
    FIRST_NAME_REQUIRED: "First name is required",
    LAST_NAME_REQUIRED: "Last name is required",
    GENDER_REQUIRED: "Gender is required",
    INCORRECT_GENDER: "Invalid gender provided(must be either 'male' or 'female')",
    INVALID_CURRENT_PASSWORD: "Incorrect current password provided",
    USER_EMAIL_AVAILABILITY_CHECK_FAILED: "Checking User email availability failed",

    // Admin Errors
    ADMIN_REGISTRATION_FAILED: "Admin registration failed",
    ADMIN_RETRIEVAL_FAILED: "Admin retrieval failed",
    ADMIN_UPDATE_FAILED: "Admin update failed",
    ADMIN_CHANGE_PASSWORD_FAILED: "Admin change password failed",
    ADMIN_ALREADY_EXISTS: "Admin with the provided email already exists",
    ADMIN_NOT_FOUND: "Admin not found",

    // Customer Errors
    CUSTOMER_REGISTRATION_FAILED: "Customer registration failed",
    CUSTOMER_RETRIEVAL_FAILED: "Customer retrieval failed",
    CUSTOMER_UPDATE_FAILED: "Customer update failed",
    CUSTOMER_CHANGE_PASSWORD_FAILED: "Customer change password failed",
    CUSTOMER_NOT_FOUND: "Customer not found",
    CUSTOMER_USERNAME_REQUIRED: "Username is required",
    CUSTOMER_WITH_EMAIL_ALREADY_EXISTS: "Customer with the provided email already exists",
    CUSTOMER_WITH_USERNAME_ALREADY_EXISTS: "Customer with the provided username already exists",

    //Item
    POST_REGISTRATION_FAILED: "Item registration failed",
    POST_RETRIEVAL_FAILED: "Item retrieval failed",
    POST_UPDATE_FAILED: "Item update failed",
    POST_NOT_FOUND: "Item not found",
    POST_TITLE_IS_REQUIRED: "Post title is required",
    POST_CONTENT_IS_REQUIRED: "Post content is required",
    ITEM_NUMBER_IN_STOCK_IS_REQUIRED: "Amount In Stock is required",
    ITEM_DESCRIPTION_IS_REQUIRED: "Item Description is required",
    ITEM_WARRANTY_PERIOD_IS_REQUIRED: "Item Warranty Period is required",

    //Category Errors
    CATEGORY_REGISTRATION_FAILED:  "Category registration failed",
    CATEGORY_RETRIEVAL_FAILED: "Category retrieval failed",
    CATEGORY_UPDATE_FAILED: "Category update failed",
    CATEGORY_DELETION_FAILED: "Category deletion failed",
    CATEGORY_NOT_FOUND: "Category not found",
    CATEGORY_NAME_IS_REQUIRED: "Category name is required",
    CATEGORY_DESCRIPTION_IS_REQUIRED: "Category description is required",
    CATEGORY_WITH_PROVIDED_NAME_EXISTS: "Category with the provided name already exists",

    //Sub Category Errors
    SUBCATEGORY_REGISTRATION_FAILED:  "Sub-category registration failed",
    SUBCATEGORY_RETRIEVAL_FAILED: "Sub-category retrieval failed",
    SUBCATEGORY_UPDATE_FAILED: "Sub-category update failed",
    SUBCATEGORY_DELETION_FAILED: "Sub-category deletion failed",
    SUBCATEGORY_NOT_FOUND: "Sub-category not found",
    SUBCATEGORY_NAME_IS_REQUIRED: "Sub-category name is required",
    SUBCATEGORY_DESCRIPTION_IS_REQUIRED: "Sub-category description is required",
    SUBCATEGORY_CATEGORY_ID_REQUIRED: "Category id is required for sub category",

    //Sub Category Admin Errors
    SUBCATEGORYADMIN_REGISTRATION_FAILED:  "Sub-category admin registration failed",
    SUBCATEGORYADMIN_RETRIEVAL_FAILED: "Sub-category admin retrieval failed",
    SUBCATEGORYADMIN_UPDATE_FAILED: "Sub-category admin update failed",
    SUBCATEGORYADMIN_DELETION_FAILED: "Sub-category admin deletion failed",
    SUBCATEGORYADMIN_NOT_FOUND: "Sub-category admin not found",
    SUBCATEGORYADMIN_CHANGE_PASSWORD_FAILED: "Sub-category admin change password failed",

    //Report Errors
    REPORT_RETRIEVE_FALED: "Report retrieval failed"
};