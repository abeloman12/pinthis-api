import {model, Schema} from 'mongoose';

let CustomerSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        lowercase: true,
        required: true,
        enum: [
            'male',
            'female'
        ]
    }
}, {collection: 'customers'});

export default model('Customer', CustomerSchema);