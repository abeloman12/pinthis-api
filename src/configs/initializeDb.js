import mongoose from 'mongoose';
import config from './config';
import logger from './logger';

export default callback => {
    let dbUri = "mongodb://" + config.db.host + ":" + config.db.port + "/" + config.db.dbname;
    let connection = mongoose.connect(dbUri, {useNewUrlParser: true});

    mongoose.connection.on('connected', () => {
        logger.info('Database connection established.');
    });

    mongoose.connection.on('error', (error) => {
        logger.info('Database connection error: ' + error);
    });

    mongoose.connection.on('disconnected', () => {
        logger.info('Database connection terminated.');
    });
}