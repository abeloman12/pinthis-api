import async from 'async';
import express from 'express';
import passport from 'passport';

import Post from '../models/Post';
import SubCategory from '../models/SubCategory';

import logger from '../configs/logger';
import config from '../configs/config';
import authMiddleware from '../middlewares/AuthMiddleware';
import { kinds, createError, logError, errorMessages } from '../configs/errors';

let router = express.Router();

/**
 * Retrieve All Posts
 */
router.get('/', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {
        Post.find({}).exec((error, posts) => {
            if (error) {
                let errorMessage = processMongooseErrors(
                    "Post",
                    errorMessages.POST_RETRIEVAL_FAILED,
                    "GET",
                    error
                );
                response.status(404).json(errorMessage);
            }
            else {
                response.json(posts);
            }
        });
    
});

/**
 * Retrieve Number of Posts
 */
router.get('/count', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin), 
    (request, response) => {

    Post.countDocuments({}, (error, postsCount) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Post",
                errorMessages.POST_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(postsCount);
        }
    });
});

/**
 * Retrieve a Post
 */
router.get('/:id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.admin, config.roles.subcategory_admin, config.roles.customer), 
    (request, response) => {
    
    Post.findById(request.params.id)
    .populate('subcategory_id').exec((error, post) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Post",
                errorMessages.POST_RETRIEVAL_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else if (!post) {
            let errorMessage = createError(
                errorMessages.POST_RETRIEVAL_FAILED,
                "GET",
                errorMessages.POST_NOT_FOUND
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.json(post);
        }
    });
});

/**
 * Retrieve Number of Items for a specific Subcategory
 */
router.get('/count/:subcategory_id', 
    // passport.authenticate('jwt', {session: false}), 
    // authMiddleware.checkAccessLevel(config.roles.customer), 
    (request, response) => {

    Post.countDocuments({subcategory_id : request.params.subcategory_id}, (error, postsCount) => {
        if (error) {
            let errorMessage = processMongooseErrors(
                "Post",
                errorMessages.POST_REGISTRATION_FAILED,
                "GET",
                error
            );
            response.status(404).json(errorMessage);
        }
        else {
            response.status(200).json(postsCount);
        }
    });
});

/**
 * Retrieve All Posts
 */
router.get('/search/:search_term', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.customer), 
    (request, response) => {
            Post.find({title: new RegExp(request.params.search_term, "i")})
            .populate('subcategory_id').exec((error, posts) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Item",
                        errorMessages.POST_RETRIEVAL_FAILED,
                        "GET",
                        error
                    );
                    response.status(400).send(errorMessage);
                }
                else {
                    response.status(200).json(posts);
                }
            });    
});

/**
 * Retrieve Posts based on SubCategory
 */
router.get('/sub_category/:subcategory_id', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.customer), 
    (request, response) => {
            Post.find({subcategory_id:request.params.subcategory_id})
            .populate('subcategory_id').exec((error, posts) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Item",
                        errorMessages.POST_RETRIEVAL_FAILED,
                        "GET",
                        error
                    );
                    response.status(400).send(errorMessage);
                }
                else {
                    response.status(200).json(posts);
                }
            });
    
});

/**
 * Create Post
 * @param  {title, content, subcategory_id, image_paths}
 */
router.post('/', 
    passport.authenticate('jwt', {session: false}), 
    authMiddleware.checkAccessLevel(config.roles.sub_admin), 
    (request, response) => {
    
    async.waterfall([
        (done) => {
            SubCategory.findById(request.body.subcategory_id).exec((error, sub_category) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "SubCategory",
                        errorMessages.SUBCATEGORY_RETRIEVAL_FAILED,
                        "GET",
                        error
                    );
                    done(errorMessage);
                }
                else if (!sub_category) {
                    let errorMessage = createError(
                        errorMessages.SUBCATEGORY_RETRIEVAL_FAILED,
                        "GET",
                        errorMessages.SUBCATEGORY_NOT_FOUND
                    );
                    done(errorMessage);
                }
                else {
                    done(null, sub_category);
                }
            });
        },
        (sub_category, done) => {
            let newPost = new Post();
            newPost.subcategory_id = sub_category._id;
            newPost.title = request.body.title;
            newPost.content = request.body.content;
            newPost.image_paths = request.body.image_paths;
            newPost.save((error, post) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Post",
                        errorMessages.POST_REGISTRATION_FAILED,
                        "POST",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    logger.info("New Post Registered");
                    return response.status(201).json(post);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error);
    })
    
});


/**
 * Update Post
 * @param  {title, content, image_paths}
 */
router.put('/:id', 
   passport.authenticate('jwt', {session: false}), 
   authMiddleware.checkAccessLevel(config.roles.sub_admin), 
    (request, response) => {

    async.waterfall([
        (done) => {    
            Post.findById(request.body._id).exec((error, post) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Post",
                        errorMessages.POST_UPDATE_FAILED,
                        "PUT",
                        error
                    );
                    done(errorMessage);
                }
                else if (!post) {
                    let errorMessage = createError(
                        errorMessages.POST_UPDATE_FAILED,
                        "PUT",
                        errorMessages.POST_NOT_FOUND
                    );
                    done(errorMessage);
                }
                else {
                    done(null, post);
                }
            });
        },
        (updatedpost, done) => {

            updatedpost.title = request.body.title;
            updatedpost.content = request.body.content;
            updatedpost.image_paths = request.body.image_paths;
            updatedpost.save((error, updatedpost) => {
                if (error) {
                    let errorMessage = processMongooseErrors(
                        "Item",
                        errorMessages.POST_UPDATE_FAILED,
                        "POST",
                        error
                    );
                    done(errorMessage);
                }
                else {
                    response.status(200).json(updatedpost);
                }
            });
        }
    ], (error) => {
        logError(error);
        return response.status(400).send(error);
    });
});


let rollback = (...items) => {
    items.forEach(item => {
        item.remove();
    });
}

let processMongooseErrors = (resource, message, method, error) => {
    let errorList = [];
    if (error.name === "ValidationError") {
        errorList = processValidationErrors(error);
    }
    else if (error.code === 11000) {
        errorList.push(errorMessages.ITEM_WITH_EMAIL_ALREADY_EXISTS);
    }
    else if (error.name === "CastError") {
        errorList.push(errorMessages.ITEM_NOT_FOUND);
    }
    else {
        errorList.push(errorMessages.UNKNOWN_ERROR);
    }
    return createError(message, method, errorList);
}

let processValidationErrors = (error) => {
    let errorList = [];
   
    if (error.errors.item_name) {
        if (error.errors.item_name.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.ITEM_NAME_IS_REQUIRED);
        }
    }

    if (error.errors.price) {
        if (error.errors.price.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.ITEM_PRICE_IS_REQUIRED);
        }
    }

    if (error.errors.number_in_stock) {
        if (error.errors.number_in_stock.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.ITEM_NUMBER_IN_STOCK_IS_REQUIRED);
        }
    }

    if (error.errors.vendor_id) {
        if (error.errors.vendor_id.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.VENDOR_ID_REQUIRED);
        }
    }

    if (error.errors.description) {
        if (error.errors.description.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.ITEM_DESCRIPTION_IS_REQUIRED);
        }
    }

    if (error.errors.warranty_period) {
        if (error.errors.warranty_period.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.ITEM_WARRANTY_PERIOD_REQUIRED);
        }else if(error.errors.warranty_period.name == 'CastError'){
            errorList.push(error.errors.warranty_period.message)
        }
    }

    if (error.errors.category_id) {
        if (error.errors.category_id.kind === kinds.REQUIRED) {
            errorList.push(errorMessages.ITEM_CATEGORY_IS_REQUIRED);
        }else if(error.errors.warranty_period.name == 'CastError'){
            errorList.push(error.errors.warranty_period.message)
        }
    }

    return errorList;
}

export default router;