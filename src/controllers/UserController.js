import express from 'express';

import User from '../models/User';

import authMiddleware from '../middlewares/AuthMiddleware';
import { createError, errorMessages } from '../configs/errors';

let router = express.Router();

router.post('/login', 
    authMiddleware.authentication, 
    authMiddleware.generateAccessToken, 
    authMiddleware.response);


/**
 * Check If Email is Available
 * @input {email}
 */
router.post('/checkEmailIsUnique', (request, response) => {
    User.findOne({email: request.body.email}, (error, user) => {
        if (error) {
            let errorMessage = createError(
                errorMessages.USER_EMAIL_AVAILABILITY_CHECK_FAILED,
                "POST",
                error
            );
            response.status(400).json(errorMessage);
        }
        else if (!user) {
            return response.status(200).send({isAvailable: true});
        }
        else {
            return response.status(200).send({isAvailable: false});
        }
    });
});

export default router;