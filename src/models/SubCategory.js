import {model, Schema} from 'mongoose';

let SubCategorySchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category_id: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }
}, {collection: 'subcategories'});

export default model('SubCategory', SubCategorySchema);